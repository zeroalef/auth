use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{
    guard, http::StatusCode, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use async_mutex::Mutex;
use deadpool_postgres::{tokio_postgres::NoTls, Runtime};
use session_manager::SessionService;

use crate::{
    api::handlers::{
        add_email, change_password, delete_email, github_oauth, google_oauth, health,
        regenerate_2fa_recovery_codes, root_health, set_2fa, set_primary_email, sign_in, sign_up,
        verify_totp,
    },
    config::{bind_address, check_env_vars, pool_config},
    crypto::CryptoUtils,
    db::{email::EmailRepository, recovery_code::RecoveryCodeRepository, user::UserRepository},
    logging::setup_logger,
    services::oauth::OAuthService,
    services::{auth::AuthService, user::UserService},
};

pub struct AppData {
    pub auth_service: Mutex<AuthService>,
    pub crypto_utils: Mutex<CryptoUtils>,
    pub oauth_service: Mutex<OAuthService>,
    pub session_service: Arc<Mutex<SessionService>>,
}

impl AppData {
    pub fn new() -> Self {
        let pool = Arc::new(Mutex::new(
            pool_config()
                .create_pool(Some(Runtime::Tokio1), NoTls)
                .unwrap(),
        ));

        let session_service = Arc::new(Mutex::new(SessionService::new_from_env()));

        Self {
            auth_service: Mutex::new(AuthService::new(
                UserRepository::new(pool.clone()),
                RecoveryCodeRepository::new(pool.clone()),
                session_service.clone(),
                UserService::new(
                    UserRepository::new(pool.clone()),
                    EmailRepository::new(pool.clone()),
                ),
            )),
            crypto_utils: Mutex::new(CryptoUtils::new()),
            oauth_service: Mutex::new(OAuthService::new(
                UserRepository::new(pool),
                SessionService::new_from_env(),
            )),
            session_service,
        }
    }
}

impl Default for AppData {
    fn default() -> Self {
        Self::new()
    }
}

async fn default_route(_req: HttpRequest) -> impl Responder {
    HttpResponse::Ok().status(StatusCode::NOT_FOUND).body(())
}

#[allow(deprecated)]
pub async fn start_app() -> std::io::Result<()> {
    check_env_vars();
    setup_logger();

    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .default_service(actix_web::web::route().to(default_route))
            .data(AppData::new())
            .service(health)
            .service(root_health)
            .service(
                web::scope("/api/v1/auth")
                    .guard(guard::Header("Content-Type", "application/json"))
                    .service(sign_in)
                    .service(sign_up)
                    .service(github_oauth)
                    .service(google_oauth)
                    .service(add_email)
                    .service(delete_email)
                    .service(set_primary_email)
                    .service(change_password)
                    .service(verify_totp)
                    .service(set_2fa)
                    .service(regenerate_2fa_recovery_codes),
            )
    })
    .bind(bind_address())?
    .run()
    .await
}
