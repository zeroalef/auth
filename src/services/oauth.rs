use std::env::var;

use actix_web::{http::StatusCode, ResponseError};
use image::storage::cloud_storage::CloudStorageUtils;
use session_manager::{SessionError, SessionService};
use thiserror::Error;
use time::{Duration, OffsetDateTime};
use uuid::Uuid;

use crate::{
    api::AuthResponse,
    config::{
        AVATARS_BUCKET_ENV, GITHUB_CLIENT_ID_ENV, GITHUB_CLIENT_SECRET_ENV, GITHUB_REDIRECT_URI_ENV,
    },
    db::{user::UserRepository, DbError},
    model::User,
    services::auth::{AUTH_COOKIE_NAME, DAYS_SESSION_VALID},
};

#[derive(Error, Debug)]
pub enum OAuthError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("GitHub error: {0}")]
    GitHub(String),
    #[error("Google error: {0}")]
    Google(String),
    #[error("Fetch error: {0:?}")]
    FetchError(#[from] reqwest::Error),
    #[error("Session error: {0:?}")]
    Session(#[from] SessionError),
    #[error("Image error: {0:?}")]
    ImageError(#[from] image::ImageError),
}

impl ResponseError for OAuthError {
    fn status_code(&self) -> StatusCode {
        match self {
            OAuthError::Session(session_error) => match session_error {
                SessionError::RedisConnection(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::Crypto(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::SessionToken(_) => StatusCode::UNAUTHORIZED,
            },
            OAuthError::GitHub(_) => StatusCode::BAD_REQUEST,
            OAuthError::Google(_) => StatusCode::BAD_REQUEST,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct OAuthService {
    user_repository: UserRepository,
    session_service: SessionService,
}

impl OAuthService {
    pub fn new(user_repository: UserRepository, session_service: SessionService) -> Self {
        Self {
            user_repository,
            session_service,
        }
    }

    async fn validate_username(&mut self, username: &mut String) -> Result<(), DbError> {
        let mut i = 0;

        while self
            .user_repository
            .find_by_username(username)
            .await?
            .is_some()
        {
            username.push_str(&format!("{}", i));
            i += 1;
        }

        Ok(())
    }

    pub async fn github(&mut self, code: &str) -> Result<AuthResponse<()>, OAuthError> {
        let client = reqwest::Client::new();

        let form_data = reqwest::multipart::Form::new()
            .text("client_id", var(GITHUB_CLIENT_ID_ENV).unwrap())
            .text("client_secret", var(GITHUB_CLIENT_SECRET_ENV).unwrap())
            .text("code", code.to_string())
            .text("redirect_uri", var(GITHUB_REDIRECT_URI_ENV).unwrap());

        let response = client
            .post("https://github.com/login/oauth/access_token")
            .header("Accept", "application/json")
            .multipart(form_data)
            .send()
            .await?;

        let data = response.bytes().await?.to_vec();
        let data = String::from_utf8(data).map_err(|_| {
            OAuthError::GitHub("GitHub sent bad response: not valid UTF-8 data".into())
        })?;

        let access_data = json::parse(&data)
            .map_err(|_| OAuthError::GitHub(format!("Can not parse GitHub response: {}", data)))?;

        let data = client
            .get("https://api.github.com/user")
            .header(
                "Authorization",
                format!("token {}", access_data["access_token"]),
            )
            .header("User-Agent", "DataAns-App")
            .send()
            .await?
            .bytes()
            .await?
            .to_vec();

        let data = String::from_utf8(data).map_err(|_| {
            OAuthError::GitHub("GitHub sent bad response: not valid UTF-8 data".into())
        })?;

        let user_data = json::parse(&data).map_err(|_| {
            OAuthError::GitHub(format!(
                "GitHub sent bad response: can not parse your profile info: {}",
                data
            ))
        })?;

        let mut username = user_data["login"]
            .as_str()
            .ok_or_else(|| {
                OAuthError::GitHub("Can not read username from your GitHub profile".into())
            })?
            .to_string();
        self.validate_username(&mut username).await?;

        let full_name = user_data["name"]
            .as_str()
            .ok_or_else(|| OAuthError::GitHub("Can not read name from your GitHub profile".into()))?
            .to_string();
        let email = user_data["email"]
            .as_str()
            .ok_or_else(|| {
                OAuthError::GitHub("Can not read email from your GitHub profile".into())
            })?
            .to_string();
        let avatar_url = user_data["avatar_url"]
            .as_str()
            .ok_or_else(|| {
                OAuthError::GitHub("Can not read avatar url from your GitHub profile".into())
            })?
            .to_string();

        let cloud_storage = CloudStorageUtils::new();
        let avatar_url = cloud_storage
            .copy_image_from_url(&avatar_url, &std::env::var(AVATARS_BUCKET_ENV).unwrap())
            .await?;

        let mut user = User {
            id: Uuid::new_v4(),
            username,
            full_name,
            avatar_url,
            joined_at: OffsetDateTime::now_utc(),
            primary_email: email,
            two_fa_secret: None,
            is_2fa_enabled: false,
        };

        if let Some(existent_user) = self
            .user_repository
            .find_by_email(&user.primary_email)
            .await?
        {
            user.id = existent_user.id;
        } else {
            self.user_repository.add(&user).await?;
        }

        let session = self
            .session_service
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        log::info!("new session: {}", session);

        let mut auth_response = AuthResponse::new(StatusCode::OK, ());
        auth_response.with_cookie(AUTH_COOKIE_NAME.to_owned(), session);

        Ok(auth_response)
    }

    pub async fn google(&mut self, code: &str) -> Result<AuthResponse<()>, OAuthError> {
        let client = reqwest::Client::new();

        let response = client
            .post("https://oauth2.googleapis.com/tokeninfo")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(format!("id_token={}", code,))
            .send()
            .await?;

        let data = response.bytes().await?.to_vec();
        let data = String::from_utf8(data).map_err(|_| {
            OAuthError::GitHub("Google sent bad response: not valid UTF-8 data".into())
        })?;

        let access_data = json::parse(&data)
            .map_err(|_| OAuthError::GitHub(format!("Can not parse Google response: {}", data)))?;

        if access_data["email_verified"] != "true" {
            return Err(OAuthError::Google("Your email is not verified".into()));
        }

        let full_name = access_data["name"]
            .as_str()
            .ok_or_else(|| OAuthError::GitHub("Can not read name from your Google profile".into()))?
            .to_string();
        let email = access_data["email"]
            .as_str()
            .ok_or_else(|| {
                OAuthError::GitHub("Can not read email from your Google profile".into())
            })?
            .to_string();
        let mut username = full_name
            .chars()
            .filter(|c| c.is_ascii_alphabetic())
            .map(|c| c.to_ascii_lowercase())
            .collect();
        self.validate_username(&mut username).await?;

        let avatar_url = access_data["picture"]
            .as_str()
            .ok_or_else(|| {
                OAuthError::GitHub("Can not read avatar url from your Google profile".into())
            })?
            .to_string();

        let cloud_storage = CloudStorageUtils::new();
        let avatar_url = cloud_storage
            .copy_image_from_url(&avatar_url, &std::env::var(AVATARS_BUCKET_ENV).unwrap())
            .await?;

        let mut user = User {
            id: Uuid::new_v4(),
            username,
            full_name,
            avatar_url,
            joined_at: OffsetDateTime::now_utc(),
            primary_email: email,
            two_fa_secret: None,
            is_2fa_enabled: false,
        };

        if let Some(existent_user) = self
            .user_repository
            .find_by_email(&user.primary_email)
            .await?
        {
            user.id = existent_user.id;
        } else {
            self.user_repository.add(&user).await?;
        }

        let session = self
            .session_service
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        log::info!("new session: {}", session);

        let mut auth_response = AuthResponse::new(StatusCode::OK, ());
        auth_response.with_cookie(AUTH_COOKIE_NAME.to_owned(), session);

        Ok(auth_response)
    }
}
