use actix_web::{http::StatusCode, ResponseError};
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
    api::types::UserResponse,
    crypto::CryptoError,
    db::{email::EmailRepository, user::UserRepository, DbError},
    model::{Email, User},
};

#[derive(Error, Debug)]
pub enum UserError {
    #[error("Bad credentials")]
    BadCredentials,
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Crypto error: {0:?}")]
    CryptoError(#[from] CryptoError),
    #[error("Session error: {0:?}")]
    Session(#[from] SessionError),
    #[error("Validation error: {0}")]
    Validation(#[from] validator::Error),
    #[error("Email {0} already used by someone")]
    AlreadyUsed(String),
    #[error("User with id {0:?} not found")]
    NotFound(Uuid),
    #[error("Email {0:?} not found")]
    EmailNotFound(String),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
    #[error("Email deletion error: {0}")]
    EmailDeletion(String),
}

impl ResponseError for UserError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserError::BadCredentials => StatusCode::UNAUTHORIZED,
            UserError::Validation(_) => StatusCode::BAD_REQUEST,
            UserError::AlreadyUsed(_) => StatusCode::BAD_REQUEST,
            UserError::EmailDeletion(_) => StatusCode::BAD_REQUEST,
            UserError::NotFound(_) => StatusCode::NOT_FOUND,
            UserError::EmailNotFound(_) => StatusCode::NOT_FOUND,
            UserError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            UserError::Session(session_error) => match session_error {
                SessionError::RedisConnection(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::Crypto(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::SessionToken(_) => StatusCode::UNAUTHORIZED,
            },
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct UserService {
    user_repository: UserRepository,
    email_repository: EmailRepository,
}

impl UserService {
    pub fn new(user_repository: UserRepository, email_repository: EmailRepository) -> Self {
        Self {
            user_repository,
            email_repository,
        }
    }

    pub async fn delete_email(
        &self,
        email: &str,
        user_id: &Uuid,
    ) -> Result<UserResponse, UserError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(UserError::NotFound(*user_id))?;

        if user.primary_email == email {
            return Err(UserError::EmailDeletion("Primary key deletion is illegal. Change primary email and then you will be able to delete this email".into()));
        }

        let email = self
            .email_repository
            .find_by_email(email)
            .await?
            .ok_or_else(|| UserError::EmailNotFound(email.into()))?;

        if email.user_id != *user_id {
            return Err(UserError::PermissionDenied(
                "User can delete only their emails questions".into(),
            ));
        }

        self.email_repository.delete_email_by_id(&email.id).await?;

        self.get_by_id(user_id).await
    }

    pub async fn get_by_id(&self, user_id: &Uuid) -> Result<UserResponse, UserError> {
        let User {
            id,
            username,
            full_name,
            avatar_url,
            joined_at,
            primary_email,
            two_fa_secret: _,
            is_2fa_enabled,
        } = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(UserError::NotFound(*user_id))?;

        let emails = self
            .email_repository
            .find_user_emails(user_id)
            .await?
            .into_iter()
            .map(|email| email.email)
            .collect();

        Ok(UserResponse {
            id,
            username,
            full_name,
            emails,
            primary_email,
            avatar_url,
            joined_at,
            is_2fa_enabled,
        })
    }

    pub async fn add_email(&self, email: &str, user_id: &Uuid) -> Result<UserResponse, UserError> {
        if (self.email_repository.find_by_email(email).await?).is_some() {
            return Err(UserError::AlreadyUsed(email.into()));
        }

        let email = Email {
            id: Uuid::new_v4(),
            email: email.into(),
            user_id: *user_id,
            added_at: OffsetDateTime::now_utc(),
        };

        self.email_repository.add_email(&email).await?;

        self.get_by_id(user_id).await
    }

    pub async fn set_primary_email(
        &self,
        email: &str,
        user_id: &Uuid,
    ) -> Result<UserResponse, UserError> {
        let email = self
            .email_repository
            .find_by_email(email)
            .await?
            .ok_or_else(|| UserError::EmailNotFound(email.into()))?;

        if email.user_id != *user_id {
            return Err(UserError::PermissionDenied(
                "You don't own this email. Please, use another one".into(),
            ));
        }

        self.user_repository
            .update_primary_email(user_id, &email.email)
            .await?;

        self.get_by_id(user_id).await
    }
}
