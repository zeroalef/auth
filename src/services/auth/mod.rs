mod sub_session;

use std::{env, sync::Arc};

use actix_web::{http::StatusCode, ResponseError};
use async_mutex::Mutex;
use image::{generate::avatar, storage::cloud_storage::CloudStorageUtils};
use libreauth::oath::TOTPBuilder;
use rand::rngs::ThreadRng;
use session_manager::{
    crypto::{decrypt_uuid, encrypt_uuid},
    SessionError, SessionService,
};
use thiserror::Error;
use time::{Duration, OffsetDateTime};
use uuid::Uuid;

use crate::{
    api::{
        types::{
            AddEmailRequest, ChangePasswordRequest, Enable2FaRequest, RecoveryCodesRequest,
            RecoveryCodesResponse, RemoveEmailRequest, SetPrimaryEmailRequest, SignInRequest,
            SignInResponse, SignUpRequest, TotpRequest, UserResponse,
        },
        AuthResponse,
    },
    config::{AVATARS_BUCKET_ENV, AVATARS_HOST},
    crypto::{CryptoError, CryptoUtils},
    db::{recovery_code::RecoveryCodeRepository, user::UserRepository, DbError},
    model::{Email, Password, RecoveryCode, UserFull},
    services::auth::sub_session::SubSession,
};

use self::sub_session::SubSessionManager;

use super::user::{UserError, UserService};

pub const AUTH_COOKIE_NAME: &str = "SessionId";
pub const DAYS_SESSION_VALID: i64 = 3;

pub const SUB_SESSION_COOKIE_NAME: &str = "SubSessionId";
pub const MINUTES_SUB_SESSION_VALID: i64 = 5;

pub const RECOVERY_CODES_AMOUNT: usize = 10;
pub const RECOVERY_CODE_BYTES_LEN: usize = 16;

#[derive(Error, Debug)]
pub enum AuthError {
    #[error("Bad credentials")]
    BadCredentials,
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Crypto error: {0:?}")]
    CryptoError(#[from] CryptoError),
    #[error("Session error: {0:?}")]
    Session(#[from] SessionError),
    #[error("Validation error: {0}")]
    Validation(#[from] validator::Error),
    #[error("User already exist")]
    UserAlreadyExist,
    #[error("{0:?}")]
    ImageError(#[from] image::ImageError),
    #[error("User error: {0:?}")]
    User(#[from] UserError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Redis error: {0:?}")]
    RedisConnection(#[from] redis::RedisError),
    #[error("2FA error: {0}")]
    TwoFA(String),
}

impl ResponseError for AuthError {
    fn status_code(&self) -> StatusCode {
        match self {
            AuthError::BadCredentials => StatusCode::UNAUTHORIZED,
            AuthError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            AuthError::Validation(_) => StatusCode::BAD_REQUEST,
            AuthError::UserAlreadyExist => StatusCode::BAD_REQUEST,
            AuthError::User(user_error) => user_error.status_code(),
            AuthError::Session(session_error) => match session_error {
                SessionError::RedisConnection(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::Crypto(e) => {
                    log::error!("{:?}", e);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                SessionError::SessionToken(_) => StatusCode::UNAUTHORIZED,
            },
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct AuthService {
    user_repository: UserRepository,
    recovery_code_repository: RecoveryCodeRepository,
    session_service: Arc<Mutex<SessionService>>,
    user_service: UserService,
    sub_sessions: SubSessionManager,
    rng: ThreadRng,
}

impl AuthService {
    pub fn new(
        user_repository: UserRepository,
        recovery_code_repository: RecoveryCodeRepository,
        session_service: Arc<Mutex<SessionService>>,
        user_service: UserService,
    ) -> Self {
        Self {
            user_repository,
            recovery_code_repository,
            session_service,
            user_service,
            sub_sessions: SubSessionManager::new("redis://127.0.0.1:6379"),
            rng: ThreadRng::default(),
        }
    }

    pub async fn sign_in(
        &mut self,
        sign_in_data: &SignInRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<AuthResponse<String>, AuthError> {
        let mut user = if sign_in_data.login.find('@').is_some() {
            self.user_repository
                .find_by_email(&sign_in_data.login)
                .await?
                .ok_or(AuthError::BadCredentials)?
        } else {
            self.user_repository
                .find_by_username(&sign_in_data.login)
                .await?
                .ok_or(AuthError::BadCredentials)?
        };

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&sign_in_data.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        if user.is_2fa_enabled {
            if user.two_fa_secret.is_none() {
                user.two_fa_secret =
                    Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
                self.user_repository.update_2fa(&user).await?;
            }
            // generate and save temporary session
            let sub_session_id = Uuid::new_v4();
            let sub_session = SubSession {
                user_id: user.id,
                expired_at: OffsetDateTime::now_utc()
                    .checked_add(Duration::minutes(MINUTES_SUB_SESSION_VALID))
                    .unwrap(),
            };

            self.sub_sessions
                .save_session(&sub_session_id.to_string(), &sub_session)
                .await?;

            let sub_session_id = encrypt_uuid(
                sub_session_id,
                &mut self.rng,
                &hex::decode(env::var(session_manager::CIPHER_KEY_ENV).unwrap()).unwrap(),
            )?;

            // send temp cookie to the client
            let mut auth_response = AuthResponse::new(
                StatusCode::OK,
                serde_json::to_string(&SignInResponse { needs_2fa: true }).unwrap(),
            );
            auth_response.with_cookie(SUB_SESSION_COOKIE_NAME.to_owned(), sub_session_id);

            return Ok(auth_response);
        }

        let session = self
            .session_service
            .lock()
            .await
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        log::info!("new session: {}", session);

        let mut auth_response = AuthResponse::new(
            StatusCode::OK,
            serde_json::to_string(&SignInResponse { needs_2fa: false }).unwrap(),
        );
        auth_response.with_cookie(AUTH_COOKIE_NAME.to_owned(), session);

        Ok(auth_response)
    }

    pub async fn verify_totp(
        &mut self,
        session_id: &str,
        data: TotpRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<AuthResponse<()>, AuthError> {
        let session_id = decrypt_uuid(
            session_id,
            &hex::decode(env::var(session_manager::CIPHER_KEY_ENV).unwrap()).unwrap(),
        )?;

        let SubSession {
            user_id,
            expired_at,
        } = self
            .sub_sessions
            .get_session(&session_id.to_string())
            .await?;

        if expired_at < OffsetDateTime::now_utc() {
            return Err(AuthError::NotAuthorized("Expired".into()));
        }

        let user = self
            .user_repository
            .find_by_id(&user_id)
            .await?
            .ok_or(AuthError::User(UserError::NotFound(user_id)))?;

        if !user.is_2fa_enabled || user.two_fa_secret.is_none() {
            return Err(AuthError::NotAuthorized(
                "2FA is not enabled for this user".into(),
            ));
        }

        let totp = TOTPBuilder::new()
            .ascii_key(user.two_fa_secret.as_ref().unwrap())
            .finalize()
            .unwrap();

        if !totp.is_valid(&data.code) {
            // check if user enter one the recovery codes
            let mut is_recovery_code = false;
            for code in self
                .recovery_code_repository
                .find_user_recovery_codes(&user_id)
                .await?
            {
                if crypto_utils.check_password(&data.code, &code.nonce, &code.hash)? {
                    is_recovery_code = true;
                }
            }

            if !is_recovery_code {
                return Err(AuthError::NotAuthorized("Invalid one time password".into()));
            }
        }

        let session = self
            .session_service
            .lock()
            .await
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        log::info!("new session: {}", session);

        let mut auth_response = AuthResponse::new(StatusCode::OK, ());
        auth_response.with_cookie(AUTH_COOKIE_NAME.to_owned(), session);

        Ok(auth_response)
    }

    pub async fn enable_2fa(
        &mut self,
        enable: Enable2FaRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::User(UserError::NotFound(*user_id)))?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&enable.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        user.is_2fa_enabled = enable.enable;

        // regenerate 2fa secret every time when user enable or reenable 2fa
        if user.is_2fa_enabled {
            user.two_fa_secret =
                Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
        }

        self.user_repository.update_2fa(&user).await?;

        Ok(self.user_service.get_by_id(user_id).await?)
    }

    pub async fn sign_up(
        &self,
        sign_up_data: &SignUpRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<AuthResponse<()>, AuthError> {
        let SignUpRequest {
            username,
            full_name,
            email,
            password,
        } = sign_up_data.clone();

        validator::user::validate_username(&username)?;
        validator::user::validate_full_name(&full_name)?;
        validator::user::validate_email(&email)?;
        validator::user::validate_password(
            &password,
            &validator::user::PasswordParameters::default(),
        )?;

        if self
            .user_repository
            .find_by_username(&username)
            .await?
            .is_some()
            || self.user_repository.find_by_email(&email).await?.is_some()
        {
            return Err(AuthError::UserAlreadyExist);
        }

        let avatar_file_name = format!(
            "{}.png",
            CryptoUtils::sha256(sign_up_data.username.as_bytes())
        );
        let storage_utils = CloudStorageUtils::new();
        storage_utils
            .save_image(
                avatar(),
                &std::env::var(AVATARS_BUCKET_ENV).unwrap(),
                &avatar_file_name,
            )
            .await?;

        let (nonce, hash) = crypto_utils.hash_password(&password)?;
        let id = Uuid::new_v4();

        let user = UserFull {
            id,
            username,
            full_name,
            password: Password { id, hash, nonce },
            avatar_url: format!("{}/{}", AVATARS_HOST, avatar_file_name),
            joined_at: OffsetDateTime::now_utc(),
            primary_email: email.clone(),
        };

        let email = Email {
            id: Uuid::new_v4(),
            email,
            user_id: user.id,
            added_at: OffsetDateTime::now_utc(),
        };

        self.user_repository
            .add_with_password_and_email(&user, &email)
            .await?;

        log::info!("User with username {} registered", user.username);

        Ok(AuthResponse::new(StatusCode::CREATED, ()))
    }

    pub async fn change_password(
        &mut self,
        passwords: ChangePasswordRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<AuthResponse<()>, AuthError> {
        let ChangePasswordRequest {
            password: old_password,
            new_password,
        } = passwords;

        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&old_password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        validator::user::validate_password(
            &new_password,
            &validator::user::PasswordParameters::default(),
        )?;

        let (nonce, hash) = crypto_utils.hash_password(&new_password)?;
        let new_password = Password {
            id: *user_id,
            hash,
            nonce,
        };

        self.user_repository.update_password(&new_password).await?;

        Ok(AuthResponse::new(StatusCode::OK, ()))
    }

    pub async fn add_user_email(
        &mut self,
        email: AddEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        validator::user::validate_email(&email.email)?;

        Ok(self.user_service.add_email(&email.email, user_id).await?)
    }

    pub async fn delete_user_email(
        &mut self,
        email: RemoveEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        Ok(self
            .user_service
            .delete_email(&email.email, user_id)
            .await?)
    }

    pub async fn set_user_primary_email(
        &mut self,
        email: SetPrimaryEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        Ok(self
            .user_service
            .set_primary_email(&email.email, user_id)
            .await?)
    }

    pub async fn regenerate_2fa_recovery_codes(
        &mut self,
        data: RecoveryCodesRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<RecoveryCodesResponse, AuthError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&data.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        if !user.is_2fa_enabled {
            return Err(AuthError::TwoFA("is not enabled".into()));
        }

        if user.is_2fa_enabled && user.two_fa_secret.is_none() {
            user.two_fa_secret =
                Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
        }

        self.recovery_code_repository.delete_codes(user_id).await?;

        let mut codes = Vec::with_capacity(RECOVERY_CODES_AMOUNT);
        for _ in 0..RECOVERY_CODES_AMOUNT {
            let code = hex::encode(&crypto_utils.random_bytes::<RECOVERY_CODE_BYTES_LEN>());

            let (nonce, hash) = crypto_utils.hash_password(&code)?;
            let recovery_code = RecoveryCode {
                id: Uuid::new_v4(),
                nonce,
                hash,
                version: 1,
                user_id: *user_id,
            };
            self.recovery_code_repository
                .save_recovery_code(&recovery_code)
                .await?;

            codes.push(code);
        }

        Ok(RecoveryCodesResponse { codes, secret: user.two_fa_secret.unwrap() })
    }
}
