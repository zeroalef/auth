use actix_web::{HttpRequest, HttpResponse, Responder};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

#[derive(Deserialize, Debug, Clone)]
pub struct SignUpRequest {
    pub username: String,
    pub email: String,
    #[serde(rename(deserialize = "fullName"))]
    pub full_name: String,
    pub password: String,
}

#[derive(Deserialize, Debug)]
pub struct SignInRequest {
    pub login: String,
    pub password: String,
}

#[derive(Deserialize, Debug)]
pub struct OAuthRequest {
    pub code: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserResponse {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub emails: Vec<String>,
    pub primary_email: String,
    pub avatar_url: String,
    #[serde(with = "rfc3339")]
    pub joined_at: OffsetDateTime,
    pub is_2fa_enabled: bool,
}

impl Responder for UserResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInResponse {
    pub needs_2fa: bool,
}

impl Responder for SignInResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize, Deserialize)]
pub struct EmailWithPassword {
    pub email: String,
    pub password: String,
}

pub type AddEmailRequest = EmailWithPassword;
pub type RemoveEmailRequest = EmailWithPassword;
pub type SetPrimaryEmailRequest = EmailWithPassword;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChangePasswordRequest {
    pub password: String,
    pub new_password: String,
}

#[derive(Serialize, Deserialize)]
pub struct TotpRequest {
    pub code: String,
}

#[derive(Serialize, Deserialize)]
pub struct Enable2FaRequest {
    pub enable: bool,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct RecoveryCodesRequest {
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct RecoveryCodesResponse {
    pub secret: String,
    pub codes: Vec<String>,
}

impl Responder for RecoveryCodesResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}
