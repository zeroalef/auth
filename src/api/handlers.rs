use actix_web::{delete, get, post, put, web, HttpRequest, HttpResponse, Responder};

use crate::{
    api::types::{
        AddEmailRequest, ChangePasswordRequest, Enable2FaRequest, OAuthRequest,
        RecoveryCodesRequest, RemoveEmailRequest, SignInRequest, SignUpRequest, TotpRequest,
    },
    app::AppData,
    services::auth::{AuthError, AUTH_COOKIE_NAME, SUB_SESSION_COOKIE_NAME},
};

use super::types::SetPrimaryEmailRequest;

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/api/v1/auth/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("auth ok.")
}

#[post("/sign-in")]
pub async fn sign_in(
    credentials: web::Json<SignInRequest>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.auth_service
        .lock()
        .await
        .sign_in(&credentials, &mut *app.crypto_utils.lock().await)
        .await
}

#[post("/2fa/totp")]
pub async fn verify_totp(
    code: web::Json<TotpRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let sub_session_cookie = req.cookie(SUB_SESSION_COOKIE_NAME).ok_or_else(|| {
        AuthError::NotAuthorized("is not authorized. No sub session cookie".into())
    })?;
    app.auth_service
        .lock()
        .await
        .verify_totp(
            sub_session_cookie.value(),
            code.into_inner(),
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[post("/2fa")]
pub async fn set_2fa(
    data: web::Json<Enable2FaRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .enable_2fa(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[post("/2fa/recovery_code")]
pub async fn regenerate_2fa_recovery_codes(
    data: web::Json<RecoveryCodesRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .regenerate_2fa_recovery_codes(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[put("/password")]
pub async fn change_password(
    data: web::Json<ChangePasswordRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .change_password(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[put("/email")]
pub async fn set_primary_email(
    data: web::Json<SetPrimaryEmailRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .set_user_primary_email(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[post("/email")]
pub async fn add_email(
    data: web::Json<AddEmailRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .add_user_email(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[delete("/email")]
pub async fn delete_email(
    data: web::Json<RemoveEmailRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.auth_service
        .lock()
        .await
        .delete_user_email(
            data.into_inner(),
            &session.user_id,
            &mut *app.crypto_utils.lock().await,
        )
        .await
}

#[post("/sign-up")]
pub async fn sign_up(
    sign_up_request: web::Json<SignUpRequest>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.auth_service
        .lock()
        .await
        .sign_up(&sign_up_request, &mut *app.crypto_utils.lock().await)
        .await
}

#[post("/github")]
pub async fn github_oauth(
    sign_up_request: web::Json<OAuthRequest>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.oauth_service
        .lock()
        .await
        .github(&sign_up_request.code)
        .await
}

#[post("/google")]
pub async fn google_oauth(
    sign_up_request: web::Json<OAuthRequest>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.oauth_service
        .lock()
        .await
        .google(&sign_up_request.code)
        .await
}
