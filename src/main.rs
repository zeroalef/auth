#[actix_web::main]
async fn main() -> std::io::Result<()> {
    auth::app::start_app().await
}
