FROM rust:latest

RUN USER=root cargo new --bin auth

WORKDIR ./auth

COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
